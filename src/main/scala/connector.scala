package org.julio.sparky_base

import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.{HBaseConfiguration, HTableDescriptor, TableName}
import org.apache.hadoop.hbase.mapreduce.TableInputFormat
import org.apache.hadoop.hbase.CellUtil
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{StructType,StructField,StringType,IntegerType,DoubleType,ByteType}


class HBaseContext(sc:org.apache.spark.SparkContext){
  def table(entry_table:String):HBaseRDD = {
    return HBaseRDD(sc, entry_table)
  }
}



object HBaseRDD {
  def apply(sc:org.apache.spark.SparkContext, entry_table:String) = {
    new HBaseRDD(sc).table(entry_table)
  }
}

class HBaseRDD(sc:org.apache.spark.SparkContext) {
  final val typeConversor = Map("string"->StringType, "int"->IntegerType, "double"->DoubleType, "bytes"->ByteType)
  val conf = HBaseConfiguration.create()
  var columns:Map[String,String] = Map() 
  var raw_RDD = sc.newAPIHadoopRDD(this.conf, classOf[TableInputFormat],
    classOf[org.apache.hadoop.hbase.io.ImmutableBytesWritable],
    classOf[org.apache.hadoop.hbase.client.Result])

  def update_base_RDD():Unit = {
    raw_RDD = sc.newAPIHadoopRDD(this.conf, classOf[TableInputFormat],
      classOf[org.apache.hadoop.hbase.io.ImmutableBytesWritable],
      classOf[org.apache.hadoop.hbase.client.Result])
  }

  def table(tabla:String):this.type = {
    this.conf.set(TableInputFormat.INPUT_TABLE, tabla)
    update_base_RDD()
    this
  }

  def select(column_input:Map[String, String]):this.type = {
    this.columns = column_input
    this.conf.set(TableInputFormat.SCAN_COLUMNS, column_input map(x=>x._1) mkString(" "))
    update_base_RDD()
    this
  }

  def asMapRDD():RDD[(String, Map[String, Any])] = {
    val splitted_columns = this.columns.map(x=>(x._1.split(":"),x._2)).map(x=>(x._1(0), x._1(1), x._2))
    (raw_RDD.map(x=> (new String(x._1.get()), splitted_columns.map(y=>(y._1+":"+y._2, CellUtil
      .cloneValue(Option(x._2.getColumnLatestCell(y._1.getBytes, y._2.getBytes))
        .getOrElse(CellUtil.createCell("missing".getBytes, "**NONEXISTANT**".getBytes))), y._3))
      .filter(_._2.deep != "**NONEXISTANT**".getBytes.deep)))
      .mapValues(x=>x.map(y=> y._3 match {
        case "string" => (y._1, Bytes.toString(y._2))
        case "int" => (y._1, Bytes.toInt(y._2))
        case "double" => (y._1, Bytes.toDouble(y._2))
        case "bytes" => (y._1, y._2)
      }))
      .mapValues(_.toMap)
      .filter(x=> !x._2.isEmpty))
  }


  def asDataFrame(sqlCtx:org.apache.spark.sql.SQLContext):org.apache.spark.sql.DataFrame = {
    val local_columns = this.columns
    val schema = StructType(StructField("id", StringType, false) :: this.columns.map(x=>StructField(x._1, this.typeConversor(x._2), true)).toList)
    val structuredRDD = asMapRDD.map(x=>Row.fromSeq(x._1 :: local_columns.map(y=>x._2.get(y._1).getOrElse(null)).toList))
    sqlCtx.createDataFrame(structuredRDD, schema)
    }
}
