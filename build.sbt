name := "HBase Lib"

version := "1.1"

scalaVersion := "2.10.5"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.5.1" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.5.1" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.5.1" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.5.1" % "provided"


resolvers += "Cloudera Maven Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/"

libraryDependencies += "org.apache.hbase" % "hbase-client" % "1.0.0-cdh5.4.7" % "provided"

libraryDependencies += "org.apache.hbase" % "hbase-common" % "1.0.0-cdh5.4.7" % "provided"

libraryDependencies += "org.apache.hbase" % "hbase-hadoop-compat" % "1.0.0-cdh5.4.7" % "provided"

libraryDependencies += "org.apache.hbase" % "hbase-hadoop2-compat" % "1.0.0-cdh5.4.7" % "provided"

libraryDependencies += "org.apache.hbase" % "hbase-server" % "1.0.0-cdh5.4.7" % "provided"



// Java netlib, que implementa BLAS para mejor rendimiento de MLlib.
// Requiere tener instaladas libblas3, liblapack3 y libgfortran3.
// En Debian/Ubuntu, haríamos: sudo apt-get install libblas3 liblapack3 libgfortran3
libraryDependencies += "com.github.fommil.netlib" % "all" % "1.1.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"

// Nombre del assembly jar que se creará
jarName in assembly := "hbase-conector.jar"

// Para evitar que assembly incluya scala en sí 
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
